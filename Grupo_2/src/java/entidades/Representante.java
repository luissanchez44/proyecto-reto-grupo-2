/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entidades;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Azucena Burgos
 */
@Entity
@Table(name = "representante")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Representante.findAll", query = "SELECT r FROM Representante r")
    , @NamedQuery(name = "Representante.findByIdRepresentante", query = "SELECT r FROM Representante r WHERE r.idRepresentante = :idRepresentante")
    , @NamedQuery(name = "Representante.findByNombresRepres", query = "SELECT r FROM Representante r WHERE r.nombresRepres = :nombresRepres")
    , @NamedQuery(name = "Representante.findByApellidosRepres", query = "SELECT r FROM Representante r WHERE r.apellidosRepres = :apellidosRepres")
    , @NamedQuery(name = "Representante.findByEdadRepres", query = "SELECT r FROM Representante r WHERE r.edadRepres = :edadRepres")
    , @NamedQuery(name = "Representante.findByParentesco", query = "SELECT r FROM Representante r WHERE r.parentesco = :parentesco")
    , @NamedQuery(name = "Representante.findByTelefono", query = "SELECT r FROM Representante r WHERE r.telefono = :telefono")})
public class Representante implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "Id_Representante")
    private Integer idRepresentante;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "Nombres_Repres")
    private String nombresRepres;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "Apellidos_Repres")
    private String apellidosRepres;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "Edad_Repres")
    private String edadRepres;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "Parentesco")
    private String parentesco;
    @Basic(optional = false)
    @NotNull
    @Column(name = "telefono")
    private int telefono;
    @JoinColumn(name = "Id_Alumno", referencedColumnName = "Id_Alumno")
    @ManyToOne
    private Alumno idAlumno;

    public Representante() {
    }

    public Representante(Integer idRepresentante) {
        this.idRepresentante = idRepresentante;
    }

    public Representante(Integer idRepresentante, String nombresRepres, String apellidosRepres, String edadRepres, String parentesco, int telefono) {
        this.idRepresentante = idRepresentante;
        this.nombresRepres = nombresRepres;
        this.apellidosRepres = apellidosRepres;
        this.edadRepres = edadRepres;
        this.parentesco = parentesco;
        this.telefono = telefono;
    }

    public Integer getIdRepresentante() {
        return idRepresentante;
    }

    public void setIdRepresentante(Integer idRepresentante) {
        this.idRepresentante = idRepresentante;
    }

    public String getNombresRepres() {
        return nombresRepres;
    }

    public void setNombresRepres(String nombresRepres) {
        this.nombresRepres = nombresRepres;
    }

    public String getApellidosRepres() {
        return apellidosRepres;
    }

    public void setApellidosRepres(String apellidosRepres) {
        this.apellidosRepres = apellidosRepres;
    }

    public String getEdadRepres() {
        return edadRepres;
    }

    public void setEdadRepres(String edadRepres) {
        this.edadRepres = edadRepres;
    }

    public String getParentesco() {
        return parentesco;
    }

    public void setParentesco(String parentesco) {
        this.parentesco = parentesco;
    }

    public int getTelefono() {
        return telefono;
    }

    public void setTelefono(int telefono) {
        this.telefono = telefono;
    }

    public Alumno getIdAlumno() {
        return idAlumno;
    }

    public void setIdAlumno(Alumno idAlumno) {
        this.idAlumno = idAlumno;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idRepresentante != null ? idRepresentante.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Representante)) {
            return false;
        }
        Representante other = (Representante) object;
        if ((this.idRepresentante == null && other.idRepresentante != null) || (this.idRepresentante != null && !this.idRepresentante.equals(other.idRepresentante))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entidades.Representante[ idRepresentante=" + idRepresentante + " ]";
    }
    
}

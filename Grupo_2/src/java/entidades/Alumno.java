/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entidades;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Azucena Burgos
 */
@Entity
@Table(name = "alumno")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Alumno.findAll", query = "SELECT a FROM Alumno a")
    , @NamedQuery(name = "Alumno.findByIdAlumno", query = "SELECT a FROM Alumno a WHERE a.idAlumno = :idAlumno")
    , @NamedQuery(name = "Alumno.findByNombresalum", query = "SELECT a FROM Alumno a WHERE a.nombresalum = :nombresalum")
    , @NamedQuery(name = "Alumno.findByApellidosalum", query = "SELECT a FROM Alumno a WHERE a.apellidosalum = :apellidosalum")
    , @NamedQuery(name = "Alumno.findByEdadalum", query = "SELECT a FROM Alumno a WHERE a.edadalum = :edadalum")
    , @NamedQuery(name = "Alumno.findByFechanacimiento", query = "SELECT a FROM Alumno a WHERE a.fechanacimiento = :fechanacimiento")
    , @NamedQuery(name = "Alumno.findByCurso", query = "SELECT a FROM Alumno a WHERE a.curso = :curso")
    , @NamedQuery(name = "Alumno.findByParalelo", query = "SELECT a FROM Alumno a WHERE a.paralelo = :paralelo")})
public class Alumno implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "Id_Alumno")
    private Integer idAlumno;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "Nombres_alum")
    private String nombresalum;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "Apellidos_alum")
    private String apellidosalum;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Edad_alum")
    private int edadalum;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Fecha_nacimiento")
    @Temporal(TemporalType.DATE)
    private Date fechanacimiento;
    @Size(max = 15)
    @Column(name = "Curso")
    private String curso;
    @Size(max = 5)
    @Column(name = "Paralelo")
    private String paralelo;
    @OneToMany(mappedBy = "idAlumno")
    private Collection<Representante> representanteCollection;

    public Alumno() {
    }

    public Alumno(Integer idAlumno) {
        this.idAlumno = idAlumno;
    }

    public Alumno(Integer idAlumno, String nombresalum, String apellidosalum, int edadalum, Date fechanacimiento) {
        this.idAlumno = idAlumno;
        this.nombresalum = nombresalum;
        this.apellidosalum = apellidosalum;
        this.edadalum = edadalum;
        this.fechanacimiento = fechanacimiento;
    }

    public Integer getIdAlumno() {
        return idAlumno;
    }

    public void setIdAlumno(Integer idAlumno) {
        this.idAlumno = idAlumno;
    }

    public String getNombresalum() {
        return nombresalum;
    }

    public void setNombresalum(String nombresalum) {
        this.nombresalum = nombresalum;
    }

    public String getApellidosalum() {
        return apellidosalum;
    }

    public void setApellidosalum(String apellidosalum) {
        this.apellidosalum = apellidosalum;
    }

    public int getEdadalum() {
        return edadalum;
    }

    public void setEdadalum(int edadalum) {
        this.edadalum = edadalum;
    }

    public Date getFechanacimiento() {
        return fechanacimiento;
    }

    public void setFechanacimiento(Date fechanacimiento) {
        this.fechanacimiento = fechanacimiento;
    }

    public String getCurso() {
        return curso;
    }

    public void setCurso(String curso) {
        this.curso = curso;
    }

    public String getParalelo() {
        return paralelo;
    }

    public void setParalelo(String paralelo) {
        this.paralelo = paralelo;
    }

    @XmlTransient
    public Collection<Representante> getRepresentanteCollection() {
        return representanteCollection;
    }

    public void setRepresentanteCollection(Collection<Representante> representanteCollection) {
        this.representanteCollection = representanteCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idAlumno != null ? idAlumno.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Alumno)) {
            return false;
        }
        Alumno other = (Alumno) object;
        if ((this.idAlumno == null && other.idAlumno != null) || (this.idAlumno != null && !this.idAlumno.equals(other.idAlumno))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entidades.Alumno[ idAlumno=" + idAlumno + " ]";
    }
    
}
